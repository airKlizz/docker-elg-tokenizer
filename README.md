## Container with a tokenizer for Estonian 
Container (docker) for tokenizer based on [ESTNLTK](https://github.com/estnltk/estnltk) with 
interface compliant with [ELG requirements](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).

## Preliminaries
* Install version control software (if you download the software from the command line), as instructed [on the git website](https://git-scm.com/)
* Install software for making / using the container, as instructed on the [docker website](https://docs.docker.com/)

## Downloading the source code
Download from [GitLab web environment](https://gitlab.com/tilluteenused/docker-elg-tokenizer.git), or use Linux command line (Windows / Mac command lines are similar):

```commandline
mkdir -p ~/gitlab-docker-elg
cd ~/gitlab-docker-elg
git clone https://gitlab.com/tilluteenused/docker-elg-tokenizer.git gitlab-docker-elg-tokenizer
```
## Building the container
Linux command line (Windows / Mac command lines are similar):
```commandline
cd ~/gitlab-docker-elg/gitlab-docker-elg-tokenizer
docker build -t tilluteenused/estnltk_tokenizer .
```

## Download image from Docker Hub
Linux command line (Windows / Mac command lines are similar):
```commandline
docker pull tilluteenused/estnltk_tokenizer
```

## Starting the container
Linux command line (Windows / Mac command lines are similar):
```commandline
docker run -p 6000:6000 tilluteenused/estnltk_tokenizer
```
Ctrl + C in a terminal window with a running container in it terminates it.

## Usage example
```commandline
curl -i --request POST --header "Content-Type: application/json" --data '{"params":{"placeholder": "app specific flags"},"type":"text","content":"Mees peeti kinni. Sarved&Sõrad"}' localhost:6000/process

HTTP/1.1 200 OK
Server: gunicorn
Date: Wed, 02 Feb 2022 18:43:37 GMT
Connection: close
Content-Type: application/json
Content-Length: 469

{"response":{"type":"annotations","annotations":{"sentence":[{"start":0,"end":17},{"start":18,"end":30}],"token":[{"start":0,"end":4,"features":{"token":"Mees"}},{"start":5,"end":10,"features":{"token":"peeti"}},{"start":11,"end":16,"features":{"token":"kinni"}},{"start":16,"end":17,"features":{"token":"."}},{"start":18,"end":24,"features":{"token":"Sarved"}},{"start":24,"end":25,"features":{"token":"&"}},{"start":25,"end":30,"features":{"token":"S\u00f5rad"}}]}}}
```
Note that the Python json library renders text in ASCII by default;
accented letters etc. are presented as Unicode codes, e.g. õ = \u00f5.

## Query json
```json
{
  "type":"text",
  "content": string, /* "The text of the request" */
  "params":{...}   /* optional */
}
```

## Response json
```json
{
  "response": {
    "type": "annotations",
    "annotations": {
      "sentence": [        /* array of sentences */
        {
          "start": 0,      /* beginning of sentence */
          "end": 17        /* end of sentence */
        }
      ],
      "token": [           /* all tokens across all sentences */
        {
          "start": number, /* beginning of token */
          "end": number,   /* end of token */
          "features": {
            "token": string /* token */
          }
        }
      ]
    }
  }
}
```

## Author
Tarmo Vaino, Heiki-Jaan Kaalep

## Licence
Copyright (c) 2021 University of Tartu and Author(s).
Software is licensed under GNU GPL V2.
