import sys
import json
import requests

'''
For testing tools:
    https://gitlab.com/tarmo.vaino/docker-elg-tokenizer
'''

if __name__ == '__main__':
    import argparse
    argparser = argparse.ArgumentParser(allow_abbrev=False)
    argparser.add_argument('-i', '--indent', action="store_true", required=False,
                           help='väljundisse taanetega json')
    args = argparser.parse_args()

    query_json = json.loads('{"params":{"placeholder": "app specific flags"},"type":"text","content":"Mees peeti kinni. Sarved&Sõrad"}')
    resp = requests.post('http://localhost:6000/process', json=query_json)
    resp_text = resp.content.decode('utf-8')
    resp_json = json.loads(resp_text)

    if args.indent is True:
        json.dump(query_json, sys.stdout, indent=4)
        print('\n----------')
        json.dump(resp_json, sys.stdout, indent=4)
    else:
        json.dump(query_json, sys.stdout)
        print('\n----------')
        json.dump(resp_json, sys.stdout)
