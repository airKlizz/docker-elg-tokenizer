## Eesti keele lausestaja-sõnestaja
[ESTNLTK-l](https://github.com/estnltk/estnltk) põhinevat lausestajat-sõnestajat sisaldav tarkvara-konteiner (docker),
mille liides vastab [ELG nõuetele](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).
Teeb utf-8 vormingus tavateksti sobivaks
[morfoloogilist analüsaatorit sisaldavale konteinerile](https://gitlab.com/tarmo.vaino/docker-elg-morf)
.

## Eeltöö
* Paigalda versioonihaldustarkvara (kui laadid tarkvara all käsurealt), juhised [git'i veebilehel](https://git-scm.com/)
* Paigalda tarkvara-konteineri tegemiseks/kasutamiseks, juhised [docker'i veebilehel](https://docs.docker.com/)

## Lähtekoodi allalaadimine
Allalaadimiseks võib kasutada [GitLab'i veebikeskkonda](https://gitlab.com/tilluteenused/docker-elg-tokenizer.git)

Linux'i käsurealt lähtekoodi allalaadimine (Windows'i/Mac'i käsurida on analoogiline):
```commandline
mkdir -p ~/gitlab-docker-elg
cd ~/gitlab-docker-elg
git clone https://gitlab.com/tilluteenused/docker-elg-tokenizer.git gitlab-docker-elg-tokenizer
```
## Konteineri ehitamine
Linux'i käsurealt konteineri ehitamine (Windows'i/Mac'i käsurida on analoogiline)
```commandline
cd ~/gitlab-docker-elg/gitlab-docker-elg-tokenizer
docker build -t tilluteenused/estnltk_tokenizer .
```

## Konteineri allalaadimine Docker Hub'ist
Allalaaditud lähtekoodist konteineri ehitamise asemel võib valmis konteineri allalaadida Docker Hub'ist.

Linux'i käsurealt konteineri allalaadimine (Windows'i/Mac'i käsurida on analoogiline)
```commandline
docker pull tilluteenused/estnltk_tokenizer
```

## Konteineri käivitamine
Linux'i käsurealt konteineri käivitamine (Windows'i/Mac'i käsurida on analoogiline):
```commandline
docker run -p 6000:6000 tilluteenused/estnltk_tokenizer
```
Pole oluline, milline on jooksev kataloog terminaliaknas konteineri käivitamise hetkel.

Käivitatud konteineri töö lõpetab Ctrl+C selles terminaliaknas, kust konteiner käivitati. 

## Kasutusnäide
```commandline
curl -i --request POST --header "Content-Type: application/json" --data '{"params":{"placeholder": "app specific flags"},"type":"text","content":"Mees peeti kinni. Sarved&Sõrad"}' localhost:6000/process

HTTP/1.1 200 OK
Server: gunicorn
Date: Wed, 02 Feb 2022 18:43:37 GMT
Connection: close
Content-Type: application/json
Content-Length: 469

{"response":{"type":"annotations","annotations":{"sentence":[{"start":0,"end":17},{"start":18,"end":30}],"token":[{"start":0,"end":4,"features":{"token":"Mees"}},{"start":5,"end":10,"features":{"token":"peeti"}},{"start":11,"end":16,"features":{"token":"kinni"}},{"start":16,"end":17,"features":{"token":"."}},{"start":18,"end":24,"features":{"token":"Sarved"}},{"start":24,"end":25,"features":{"token":"&"}},{"start":25,"end":30,"features":{"token":"S\u00f5rad"}}]}}}
```
Tasub tähele panna, et Python'i json'i teek esitab teksti vaikimisi ASCII kooditabelis; 
täpitähed jms esitatakse Unicode'i koodidena, nt. õ = \u00f5.

## Päringu json-kuju
```json
{
  "type":"text",
  "content": string, /* "The text of the request" */
  "params":{...}   /* optional */
}
```
## Vastuse json-kuju
```json
{
  "response": {
    "type": "annotations",
    "annotations": {
      "sentence": [        /* lausete massiiv */
        {
          "start": 0,      /* lause alguspositsioon */
          "end": 17        /* lause lõpupositsioon */
        }
      ],
      "token": [           /* kõigi lausete kõigi sõnede massiiv */
        {
          "start": number, /* sõne alguspositsioon */
          "end": number,   /* sõne  lõpupositsioon */
          "features": {
            "token": string /* sõne */
          }
        }
      ]
    }
  }
}
```
## Autor
Tarmo Vaino, Heiki_jaan Kaalep
## Litsents
Copyright (c) 2021 University of Tartu and Author(s).
Software is licensed under GNU GPL V2
